﻿using System.Linq;

namespace Store.Memory
{
    public class BookRepository : IBookRepository
    {
        private readonly Book[] books = new[]
        {
            new Book(1, "ISBN 12312-31231", "D. Knuth",
                "Art of Programming, Vol. 1", "This volume begins with basic programming concepts and techniques, then focuses more particularly on information structures-the representation of information inside computer, the structural relationships between data elements and how to deal with them efficiently.",
                7.19m),
            new Book(2, "ISBN 12312-31232", "M. Fowler",
                "Refactoring", "As the application of object technology--particularly the Java",
                12.45m),
            new Book(3, "ISBN 12312-31233", "B. Kernighan, D. Ritchie",
                "C Programming Language", "Known as the bible of C, this classic bestseller introduces the",
                14.98m)
        };

        public Book[] GetAllByIsbn(string isbn)
        {
            return books.Where(book => book.Isbn == isbn).ToArray();
        }

        public Book[] GetAllByTitleOrAuthor(string titleOrAuthor)
        {
            return books.Where(book => book.Title.Contains(titleOrAuthor)
                                    || book.Author.Contains(titleOrAuthor))
                        .ToArray();
        }

        public Book GetById(int id)
        {
            try {
                return books.Single(book => book.Id == id);
            }
            catch
            {
                return new Book(0, "", "", "", "", 0m);
            }
        }
    }
}
